.PHONY: cppcheck
cppcheck:
	@cd .. && cppcheck test/test.c --template={file}:{line}:{column}:{severity}:{id}:{message}