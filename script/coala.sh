#!/bin/sh
docker run -ti --rm -v "$(pwd)":/workdir --workdir=/workdir registry.hub.docker.com/evbfirmware/coala_evbox:latest coala "$@"