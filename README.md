# evblint
Evblint is a linter and formatter for code in EVBox style. Practically, it is a configuration and collection of plugins for [Coala](https://coala.io). This is an open source wrapper tool around other linters and formatters.

## Index
- [Functionality](#functionality)
- [Usage requirements](#usage-requirements)
- [Installation and usage](#installation-and-usage)
- [Coala using docker](#coala-using-docker)
- [Coala TLDR](#coala-tldr)
- [Continuous integration](#continuous-integration)
- [Future features](#future-features)

## Functionality
- C formatting (GNU Indent)
- Shell formatting & linting (shfmt, ShellCheck)
- Lua formatting & linting (LuaFormatter, Luacheck)
- Python formatting & linting (PEP8, PyFlakes)

## Usage requirements
- Either `Docker` or `native Coala`

## Installation and usage
- Copy the `.coafile` to your project. This file describes what and how the tool is going to process. You can edit this file to your needs, but the preset matches the default EVBox style.
- [Use docker to run Coala](#coala-using-docker) or run it natively. See [Coala TLDR](#coala-tldr) for a quick Coala usage.
- Each plugin used by Coala has specific settings. Some of these can be configured in the `.coafile`, but sometimes the plugin is incomplete or requires extra files. You might need to edit to your needs. The following plugins use a config file:
    - LuaLintBear
    - LuaFormatterBear

## Coala using Docker
The command to run coala in docker **on linux** is as follows:
```shell
docker run -ti --rm -v "$(pwd)":/workdir --workdir=/workdir registry.hub.docker.com/evbfirmware/coala_evbox:latest coala <your arguments>
```
You can also use the `script/coala.sh`, which does exactly the same.
```shell
coala.sh <your arguments>
```

## Coala TLDR
Coala uses plugins for linting and formatting. These are called 'bears'.

Important arguments for Coala are are:

| arguments                       | result                                                        | example                             |
| ------------------------------- | ------------------------------------------------------------- | ----------------------------------- |
| -h                              | Coala help                                                    | coala -h                            |
| [blank]                         | Run everything                                                | coala                               |
| &lt;section&gt; ...             | Run one or more specific config file sections                 | coala all.sh.format                 |
| --filter-by section_tags &lt;tag&gt; | [Run all sections and their children with the given tag](#section-tags) | coala --filter-by section_tags python |
| --no-orig                       | Prevent creation of *.orig files after applying patch         | coala --no-orig                     |
| [--ci](#continuous-integration) | Just check if fixes are required                              | coala --ci                          |
| -B [--show-details] [-b &lt;bear_name&gt;] | List all or a specific bears, optionally with info | coala -B --show-details -b PEP8Bear |

### Section tags
An example of a section tag is:
```
[all.python]
tags = python # the tag

[all.python.format]
bears = someformatter
```

Child sections inherit this tag. Filtering by tag thus also includes child section. The following runs both `all.python` and `all.python.format`:
```
coala --filter-by section_tags python
```

## Continuous integration
Coala can provide feedback on if any fixes are required, without making any changes or requiring user input. For this use the `--ci` flag.

## Future features
- C linting (CppCheck?)
- Convenience scripts
- Move LuaFormatter and LuaCheck config files into .coafile

## Known bugs
- Formatting does not always add newline. At least for shell formatting and lua formatting if the newline would be the only change.
- Shell linting hides the 'INFO' messages from ShellCheck