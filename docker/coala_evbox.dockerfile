ARG coala_defaultbears

# get shfmt
FROM registry.hub.docker.com/mvdan/shfmt

# actual base image
FROM ${coala_defaultbears}

# copy shfmt
COPY --from=0 /bin/shfmt /bin/shfmt

# install lua formatter
RUN \
    apk add git make cmake && \
    apk del g++ && \
    apk add g++ --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main && \
    git clone --recurse-submodules https://github.com/Koihik/LuaFormatter.git /tmp/luafmt && \
    cd /tmp/luafmt && \
    mkdir build && \
    cd build && \
    cmake -DBUILD_TESTS=OFF -DCOVERAGE=OFF .. && \
    cmake --build . --target install && \
    rm -rf /tmp/luafmt && \
    apk del git make cmake g++ && \
    rm -rf /var/cache/apk/*
RUN apk add libstdc++

COPY bears/* /coala-bears/bears/evbox/
RUN touch /coala-bears/bears/evbox/__init__.py && \
    pip3 install -U /coala-bears
