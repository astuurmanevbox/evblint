ARG coala_bare

FROM ${coala_bare}

ARG branch=master
RUN echo branch=$branch

# packages required for installation
RUN apk add \
    git \
    g++ \
    python3-dev \
    musl-dev \
    libffi-dev \
    libxml2-dev \
    libxslt-dev

# clone bears
RUN git clone --depth 1 --branch=$branch https://github.com/coala/coala-bears.git

# install bears
RUN pip3 install --no-cache-dir ./coala-bears

# remove installation packages
RUN apk del \
    git \
    g++ \
    python3-dev \
    musl-dev \
    libffi-dev \
    libxml2-dev \
    libxslt-dev

# remove apk cache
RUN rm -rf /var/cache/apk/*

# install bear dependencies
RUN apk add \
    indent \
    luacheck

# enable community packages
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk update
RUN apk add -U \
    shellcheck \
    cppcheck

# remove apk cache
RUN rm -rf /var/cache/apk/*
