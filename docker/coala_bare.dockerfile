FROM alpine:latest

ARG branch=master
RUN echo branch=$branch

WORKDIR /

RUN apk update && apk upgrade

# packages required for installation
RUN apk add \
    git \
    python3

# clone coala
RUN git clone --depth 1 --branch=$branch https://github.com/coala/coala.git coala

# install coala
RUN pip3 install --no-cache-dir ./coala

# remove clone dir
RUN rm -rf coala

# remove installation packages
RUN apk del \
    git

# remove apk cache
RUN rm -rf /var/cache/apk/*