[all]

[all.c]
tags = c

[all.c.lint]
bears = CppcheckFromCommand
command = make -C test -f test.mk cppcheck --no-print-directory

[all.c.format]
files = **.c
bears = GNUIndentBear
linux_style = true
max_line_length = 120

[all.sh]
files = **.sh
tags = sh

[all.sh.lint]
bears = ShellCheckBear

[all.sh.format]
bears = ShfmtBear
lang_variant = posix
indent_switch = true
space_redirect = true
indent = 0

[all.lua]
files = **.lua
tags = lua

[all.lua.lint]
bears = LuaLintBear

# having two steps of formatting as a workaround for a LuaFormatter bug.
# makes sure the normalizer runs first
[all.lua.format.step1]
bears = LuaMultilineCommentNormalizer

[all.lua.format.step2]
bears = LuaFormatterBear
column_width = 120
indent_width = 4
continuation_indent_width = 4
use_tab = false
keep_simple_block_one_line = false
align_args = true
chop_down_parameter = true
break_after_functioncall_lp = true
break_before_functioncall_rp = true
align_parameter = true
break_after_functiondef_lp = true
break_before_functiondef_rp = true
align_table_field = true
break_after_table_lb = true
break_before_table_rb = true
chop_down_kv_table = true
table_sep = ,
extra_sep_at_table_end = false
break_after_operator = true

[all.python]
files = **.py
tags = python

[all.python.format]
bears = PEP8Bear

[all.python.lint]
bears = PyFlakesBear