from coalib.bearlib.abstractions.Linter import linter
from dependency_management.requirements.DistributionRequirement import (
    DistributionRequirement)


@linter(executable='lua-format',
        output_format='corrected',
        result_message='Formatting can be improved.'
        )
class LuaFormatterBear:
    """
    Checks and corrects Lua formatting using LuaFormatter.

    See https://github.com/Koihik/LuaFormatter
    """

    LANGUAGES = {'lua'}
    REQUIREMENTS = {DistributionRequirement('lua-format')}
    AUTHORS = {'Aart Stuurman'}
    AUTHORS_EMAILS = {'aart.stuurman@ev-box.com'}
    LICENSE = {'GNU GPLv3'}
    CAN_FIX = {'Formatting'}
    SEE_MORE = 'https://github.com/Koihik/LuaFormatter'

    @staticmethod
    def generate_config(filename, file,
                        column_width: int = 80,
                        indent_width: int = 4,
                        continuation_indent_width: int = 4,
                        use_tab: bool = False,
                        keep_simple_block_one_line: bool = True,
                        align_args: bool = True,
                        break_after_functioncall_lp: bool = False,
                        break_before_functioncall_rp: bool = False,
                        align_parameter: bool = True,
                        chop_down_parameter: bool = False,
                        break_after_functiondef_lp: bool = False,
                        break_before_functiondef_rp: bool = False,
                        align_table_field: bool = True,
                        break_after_table_lb: bool = True,
                        break_before_table_rb: bool = True,
                        chop_down_kv_table: bool = True,
                        table_sep: str = ',',
                        extra_sep_at_table_end: bool = False,
                        break_after_operator: bool = True):
        """
        :param column_width: The column limit of one line.
        :param indent_width: The number of spaces used for indentation.
        :param continuation_indent_width: Indent width for continuations line.
        :param use_tab: bool = Use tabs for indentation.
        :param keep_simple_block_one_line: Allow format simple block to one line.
        :param align_args: Align arguements of function call if there is a line break. If false, use continuation_indent_width to indentation.
        :param break_after_functioncall_lp: Break after '(' of function call if columns greater than column_limit.
        :param break_before_functioncall_rp: Break before ')' of function call if columns greater than column_limit.
        :param align_parameter: Align parameter of function define if there is a line break. if false, use continuation_indent_width to indentation.
        :param chop_down_parameter: Chop down all parameters if the function declaration doesn’t fit on a line.
        :param break_after_functiondef_lp: Break after '(' of function define if columns greater than column_limit.
        :param break_before_functiondef_rp: Break before ')' of function define if columns greater than column_limit.
        :param align_table_field: Align fields of table if there is a line break. if false, use indent_width to indentation.
        :param break_after_table_lb: Break after '{' of table if columns greater than column_limit.
        :param break_before_table_rb: Break before '}' of table if columns greater than column_limit.
        :param chop_down_kv_table: Chop down table if table contains key.
        :param table_sep: Define character to separate table fields.
        :param extra_sep_at_table_end: Add a extra field separator after last field unless the table is in a single line.
        :param break_after_operator: Put break after operators if columns greater than column_limit. If false, put break before operators.
        """

        config_file = \
            "column_limit: " + str(column_width) + "\n" + \
            "indent_width: " + str(indent_width) + "\n" + \
            "continuation_indent_width: " + str(continuation_indent_width) + "\n" + \
            "use_tab: " + ("true" if use_tab else "false") + "\n" + \
            "keep_simple_block_one_line: " + ("true" if keep_simple_block_one_line else "false") + "\n" + \
            "align_args: " + ("true" if align_args else "false") + "\n" + \
            "break_after_functioncall_lp: " + ("true" if break_after_functioncall_lp else "false") + "\n" + \
            "break_before_functioncall_rp: " + ("true" if break_before_functioncall_rp else "false") + "\n" + \
            "align_parameter: " + ("true" if align_parameter else "false") + "\n" + \
            "chop_down_parameter: " + ("true" if chop_down_parameter else "false") + "\n" + \
            "break_after_functiondef_lp: " + ("true" if break_after_functiondef_lp else "false") + "\n" + \
            "break_before_functiondef_rp: " + ("true" if break_before_functiondef_rp else "false") + "\n" + \
            "align_table_field: " + ("true" if align_table_field else "false") + "\n" + \
            "break_after_table_lb: " + ("true" if break_after_table_lb else "false") + "\n" + \
            "break_before_table_rb: " + ("true" if break_before_table_rb else "false") + "\n" + \
            "chop_down_kv_table: " + ("true" if chop_down_kv_table else "false") + "\n" + \
            "table_sep: '" + table_sep + "'" + "\n" + \
            "extra_sep_at_table_end: " + ("true" if extra_sep_at_table_end else "false") + "\n" + \
            "break_after_operator: " + ("true" if break_after_operator else "false") + "\n"
        return config_file

    @staticmethod
    def create_arguments(filename, file, config_file
                         ):
        args = []
        args.append('-so')

        args.append('-c')
        args.append(config_file)

        args.append(filename)

        return args
