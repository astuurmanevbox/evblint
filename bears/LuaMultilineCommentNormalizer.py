import re

from coalib.bears.LocalBear import LocalBear
from coalib.results.Diff import Diff


class LuaMultilineCommentNormalizer(LocalBear):

    def run(self, filename, file):
        """
        Replaces all occurences of '--]]' with '--]] --' if they are the last token on a line.
        """

        LANGUAGES = {'lua'}
        AUTHORS = {'Aart Stuurman'}
        AUTHORS_EMAILS = {'aart.stuurman@ev-box.com'}
        LICENSE = {'GNU GPLv3'}
        CAN_FIX = {'Formatting'}

        linenumber = 1
        for line in file:
            normalized, isreplaced = re.subn(r'--]](\s*)$', '--]] --', line)
            if isreplaced:
                diff = Diff(file)
                diff.modify_line(linenumber, normalized)
                yield self.new_result(message="Can replace '--]]' with '--]] --'. This prevents a bug in LuaFormatterBear.",
                                      file=filename,
                                      line=linenumber,
                                      diffs={filename: diff})
            linenumber += 1
