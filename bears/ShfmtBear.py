from coalib.bearlib.abstractions.Linter import linter
from dependency_management.requirements.DistributionRequirement import (
    DistributionRequirement)


@linter(executable='shfmt',
        output_format='corrected',
        result_message='Formatting can be improved.'
        )
class ShfmtBear:
    """
    Checks and corrects shell formatting using shfmt.
    Supports bash, posix, and mksh.
    Does not support maximum line length.

    See https://github.com/mvdan/sh
    """

    LANGUAGES = {'sh'}
    REQUIREMENTS = {DistributionRequirement('shfmt')}
    AUTHORS = {'Aart Stuurman'}
    AUTHORS_EMAILS = {'aart.stuurman@ev-box.com'}
    LICENSE = {'GNU GPLv3'}
    CAN_FIX = {'Formatting'}
    SEE_MORE = 'https://github.com/mvdan/sh'

    @staticmethod
    def create_arguments(filename, file, config_file,
                         simplify: bool = False,
                         lang_variant: str = 'bash',
                         indent: int = 0,
                         indent_switch: bool = False,
                         space_redirect: bool = False,
                         keep_paddings: bool = False,
                         minify: bool = False
                         ):
        """
        :param simplify: simplify the code.
        :param lang_variant: bash, posix, or mksh.
        :param indent: 0 for 0 for tabs, >0 for number of spaces..
        :param indent_switch: switch cases will be indented.
        :param space_redirect: redirect operators will be followed by a space.
        :param keep_paddings: keep column alignment paddings.
        :param minify: minify program to reduce its size (implies simplify code).
        """
        args = []
        args.append('-ln')
        args.append(lang_variant)

        args.append('-i')
        args.append(str(indent))

        if indent_switch:
            args.append('-ci')

        if space_redirect:
            args.append('-sr')

        if keep_paddings:
            args.append('-kp')

        if minify:
            args.append('-mn')
        elif simplify:
            args.append('-s')

        args.append(filename)

        return args
